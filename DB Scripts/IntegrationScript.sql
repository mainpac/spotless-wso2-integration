/*********    PLEASE CHANGE THESE URLs BASED ON YOUR ENVIRONMENT     *************/
DECLARE @DispatchServiceURL nvarchar(250) = 'http://10.8.0.4/Spotless_DevServices/DispatchService.svc/DispatchServiceHttp'
DECLARE @EventListStandardServiceURL nvarchar(250) = 'http://10.8.0.4/Spotless_DevServices/EventListStandard.svc'
DECLARE @WSO2ServiceURL nvarchar(250) ='http://10.8.0.5:8280/services/services/ExportEventList'



DECLARE @mainpacService nvarchar(100) = 'Export EventList Standard'
DECLARE @wso2Service nvarchar(100) = 'Export EventList WSO2'


--set dispatcher service
UPDATE GlobalSettings SET SettingName = @DispatchServiceURL WHERE SettingID = 1028

--set processing and wso service
EXEC integration.CreateExportEventListStandardEndpoint  @mainpacService, @EventListStandardServiceURL
EXEC integration.CreateExternalServiceEndpoint @wso2Service, @WSO2ServiceURL

EXEC integration.CreateEventDefinition  @entityName='WorkOrder' , @eventType='StatusChange' ,@customAction=null, @processingServiceName=@mainpacService, @externalServiceName=@wso2Service , @name= 'Export Completed WorkOrder' , @entityStatus = 'Completed'
--not required more
--EXEC integration.CreateEventDefinition  @entityName='WorkOrder' , @eventType='Insert' ,@customAction=null, @processingServiceName=@mainpacService, @externalServiceName=@wso2Service , @name= 'Export New WorkOrder' 

EXEC integration.CreateEventDefinition  'ReceiptLine' , 'Custom' ,'ReceiptCommit', @mainpacService, @wso2Service , 'Export Commit Receipt' --<--optional name , otherwise concatenates 'Export ' + entityname + action
EXEC integration.CreateEventDefinition  'ReceiptLine' , 'Custom' ,'ReceiptCommitDocketNumber', @mainpacService, @wso2Service , 'Export Commit Receipt' --<--optional name , otherwise concatenates 'Export ' + entityname + action



select * from EventDefinitionDetail
select * from [dbo].[EventRegistrationDetail]
select * from [dbo].[EventRegistrationDefinitionDetail]

